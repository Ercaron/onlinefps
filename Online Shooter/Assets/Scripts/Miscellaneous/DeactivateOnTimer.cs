﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateOnTimer : MonoBehaviour
{
    [SerializeField] float _time;
    private void OnEnable()
    {
        Invoke("Deactivate", _time);
    }

   
    void Deactivate()
    {
        CancelInvoke();
        gameObject.SetActive(false);
    }
}
