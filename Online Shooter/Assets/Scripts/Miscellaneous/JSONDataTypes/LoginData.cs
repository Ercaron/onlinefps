﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginData
{
    public int error;
    public string description;
    public int kills;
}
