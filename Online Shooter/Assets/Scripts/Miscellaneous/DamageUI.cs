﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro; 

public class DamageUI : MonoBehaviour
{
    [SerializeField] float _maxScale;
    [SerializeField] PhotonView _view;
    [SerializeField] TextMeshProUGUI _tmpro;

    public Transform Follow { get; set; }
    public PhotonView View { get => _view; set => _view = value; }

    void Start()
    {
        View.GetComponent<PhotonView>();
        StartCoroutine("ChangeScale");
        if (PhotonNetwork.IsMasterClient) Invoke("DestroyObject", 1.25f);
    }

    private void Update()
    {
        Vector3 v = Camera.main.transform.position - transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(Camera.main.transform.position - v);
        transform.Rotate(45, 180, 0);

        if (!PhotonNetwork.IsMasterClient) return;

        transform.position += transform.up * 1.5f * Time.deltaTime;
        if (Follow)
            transform.position = new Vector3(Follow.position.x, transform.position.y, Follow.position.z+1f);

        
    }

    IEnumerator ChangeScale()
    {
        while (_tmpro.fontSize < _maxScale)
        {
                _tmpro.fontSize += 0.01f;
               //transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
               yield return new WaitForSeconds(0.05f);
        }

    }

    void DestroyObject()
    {
        PhotonNetwork.Destroy(this.gameObject);
    }


    [PunRPC]
    public void ChangeValue(int amount)
    {
        _tmpro.text = amount.ToString();
    }
}
