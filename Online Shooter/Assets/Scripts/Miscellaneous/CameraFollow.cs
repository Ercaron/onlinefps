﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;


public class CameraFollow : MonoBehaviourPun
{    

    [SerializeField] Vector3 _baseDistance;    
    [SerializeField] GameObject _followObject;
    Vector3 _previosPos;    


    void Start()    
    {
        Camera.main.transform.position = transform.localPosition;
     }      



     void Update()   
     {     
         if (_followObject.transform.position != _previosPos)
         {         
             Camera.main.transform.position = _followObject.transform.position + _baseDistance; 
             _previosPos = _followObject.transform.position;       
         } 
     }
 }