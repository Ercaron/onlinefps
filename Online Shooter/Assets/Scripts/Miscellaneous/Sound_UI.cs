﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Voice.Unity;
using Photon.Pun;
using Photon.Realtime;
public class Sound_UI : MonoBehaviourPun
{
    Speaker _speaker;
    //public Image _soundImagePrefab;
    [SerializeField] GameObject _soundImagePrefab;
    GameObject _ui;
    public Vector3 offset = new Vector3(0, 1.5f, 0);
    Recorder _rec;

    [SerializeField] GameObject parent;

    Player player;

    public Player Player { get => player; set => player = value; }

    void Start()
    {
        _rec = FindObjectOfType<Recorder>();
        _ui = Instantiate<GameObject>(_soundImagePrefab);
        _speaker = GetComponent<Speaker>();
        _ui.transform.parent = parent.transform;
        _ui.transform.position = Vector3.zero;
    }
    void Update()
    {
        _ui.transform.position = transform.position + offset;

        Vector3 v = Camera.main.transform.position - _ui.transform.position;
        v.x = v.z = 0.0f;
        _ui.transform.LookAt(Camera.main.transform.position - v);
        _ui.transform.Rotate(45, 180, 0);


        if (PhotonNetwork.LocalPlayer == player)
        {
            if (_rec.IsCurrentlyTransmitting)
            {
                _ui.gameObject.SetActive(true);
            }
            else
            {
                _ui.gameObject.SetActive(false);
            }
        }
        else
        {
            if (_speaker.IsPlaying)
            {
                _ui.gameObject.SetActive(true);
            }
            else
            {
                _ui.gameObject.SetActive(false);
            }
        }
    }
}
