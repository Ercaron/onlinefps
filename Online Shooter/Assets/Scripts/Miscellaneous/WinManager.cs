﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System.Linq;

public class WinManager : MonoBehaviourPunCallbacks
{
    public static WinManager Instance;

    Dictionary<string, int> _killsPerPlayer;

    string winObjPath = "UI/WinnerUI";

    WinnerFollow winnerObj;

    string _clientName;

    public string ClientName { get => _clientName; set => _clientName = value; }

    void Awake()
    {
        if (Instance != null) { Destroy(this); }
        else Instance = this;

        _killsPerPlayer = new Dictionary<string, int>();
    }

    public void AddPlayer(int playerID)
    {
        if (!PhotonNetwork.IsMasterClient) return;

        PhotonView playerView = PhotonView.Find(playerID);
        Character character = playerView.GetComponent<Character>();
        if (!_killsPerPlayer.ContainsKey(character.Name))
        {
            _killsPerPlayer.Add(character.Name, 0);
        }
    }

    public void RemovePlayer(string playerToRemove)
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (_killsPerPlayer != null &&  _killsPerPlayer.ContainsKey(playerToRemove))
        {
            _killsPerPlayer.Remove(playerToRemove);
        }
    }

    public void PlayerKilled(int killedID, int killerID)
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (killerID == 0 || killedID == 0) return;
        PhotonView killedView = PhotonView.Find(killedID);
        PhotonView killerView = PhotonView.Find(killerID);

        Character killerCharacter = killerView.GetComponent<Character>();
        Character killedCharacter = killedView.GetComponent<Character>();

        _killsPerPlayer[killedCharacter.Name] = _killsPerPlayer[killedCharacter.Name] - 1;
        _killsPerPlayer[killerCharacter.Name] = _killsPerPlayer[killerCharacter.Name] + 1;

        if (_killsPerPlayer[killedCharacter.Name] < 0) _killsPerPlayer[killedCharacter.Name] = 0;

        if (_killsPerPlayer[killerCharacter.Name] >= 5) Win(killerID);
    }

    public void Win(int winnerID)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        PhotonView winnerView = PhotonView.Find(winnerID);
        Character _char = winnerView.GetComponent<Character>();
        
        if(winnerObj == null)
        {
            winnerObj = PhotonNetwork.Instantiate(winObjPath, Vector3.zero, Quaternion.identity).GetComponent<WinnerFollow>();  
        }

        winnerObj.Follow = _char.transform;


        photonView.RPC("InformWin", RpcTarget.AllBuffered, winnerID);

        foreach (var item in _killsPerPlayer.Keys.ToList())
        {
            _killsPerPlayer[item] = 0;
        }
    }


    [PunRPC]
    public void InformWin(int id)
    {
        PhotonView winner = PhotonView.Find(id);
        Character _char = winner.GetComponent<Character>();

        if (_char.Name == ClientName)
        {
            ChatManager.Instance.WriteOnConsole("<color=yellow><b>Has ganado!</b></color>");
        }
        else
        {
            ChatManager.Instance.WriteOnConsole("<color=yellow><b>Lo lamento, has perdido. El ganador ha sido " + _char.Name + "</color>");
        }
    }

}
