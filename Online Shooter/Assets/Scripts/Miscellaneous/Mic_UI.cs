﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice.Unity;
public class Mic_UI : MonoBehaviour
{
    Recorder _rec;
    public Sprite micOn;
    public Sprite micOff;
    public Image micUI;
    bool _lastvalue;

    void Start()
    {
        _rec = FindObjectOfType<Recorder>();
    }
    void Update()
    {
        if (_rec.TransmitEnabled != _lastvalue)
        {
            _lastvalue = _rec.TransmitEnabled;
            if (_lastvalue)
            {
                micUI.sprite = micOn;
            }
            else
            {
                micUI.sprite = micOff;
            }
        }
    }
}
