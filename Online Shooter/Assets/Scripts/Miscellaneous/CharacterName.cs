﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class CharacterName : MonoBehaviour
{


    [SerializeField] TextMeshProUGUI _playerName;
    [SerializeField] Character character;
    
    void Update()
    {
        if(_playerName.text.Length == 0) _playerName.text = character.Name;


        Vector3 v = Camera.main.transform.position - transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(Camera.main.transform.position - v);
        transform.Rotate(45, 180, 0);
    }
}
