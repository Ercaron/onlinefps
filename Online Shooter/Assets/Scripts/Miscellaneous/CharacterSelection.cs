﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using System.Diagnostics;
using System.IO;

public class CharacterSelection : MonoBehaviourPun
{
    [SerializeField] GameObject _charSelectionMenu;
    [SerializeField] List<Button> buttons;
    [SerializeField] InputField _nickname;

    string _previousClass;
    public string SelectedClass { get; set; }


    
    void Start()
    {
        buttons[0].onClick.AddListener(ChooseMage);
        buttons[1].onClick.AddListener(ChooseKnight);
        SelectedClass = "Classes/Mage";

        if(PhotonNetwork.LocalPlayer.NickName != null && PhotonNetwork.LocalPlayer.NickName != string.Empty)
        {
            _nickname.text = PhotonNetwork.LocalPlayer.NickName;
            _nickname.interactable = false;
        }
    }


    void ChooseMage()
    {
        SelectedClass = "Classes/Mage";
    }

    void ChooseKnight()
    {
        SelectedClass = "Classes/Knight";
    }

    public void SelectedNewClass()
    {
        _previousClass = SelectedClass;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape)) 
        {
            _charSelectionMenu.SetActive(!_charSelectionMenu.activeInHierarchy);
            if (SelectedClass != _previousClass) SelectedClass = _previousClass;
        } 
    }


}
