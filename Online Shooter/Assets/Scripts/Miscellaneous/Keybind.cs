﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Keybind : MonoBehaviour
{
    public string ActionName;
    public Button Button;
    public TMP_Text ActionText;
    public TMP_Text KeyText;

}
