﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class SpellDurationUI : MonoBehaviour
{
    Image _image;
    float _duration;
    float _actualTime;
    Vector3 _offset;

    PhotonView _view;


   // public Transform Follow { get; set; }
    public float Duration { get => _duration; set { _duration = value; } }

    public float ActualTime { get => _actualTime; set => _actualTime = value; }
    public PhotonView View { get => _view; set => _view = value; }

    public Transform Follow { get; set; }
    public Vector3 Offset { get => _offset; set => _offset = value; }

    private void Awake()
    {
        _image = GetComponentInChildren<Image>();
        View = GetComponent<PhotonView>();

    }
    void Start()
    {
        
        StartCoroutine("DecreaseBar");
        if (PhotonNetwork.IsMasterClient) StartCoroutine("SendTimeUpdate");
        
    }

    private void Update()
    {
        if (Follow)
            transform.position = Follow.transform.position + Offset;
        //Vector3 v = Camera.main.transform.position - transform.position;
        //v.x = v.z = 0.0f;
        //transform.LookAt(Camera.main.transform.position - v);
        //transform.Rotate(45, 180, 0);


    }

    IEnumerator DecreaseBar()
    {
        _image.fillAmount = 1;
        while(_image.fillAmount > 0)
        {
            _image.fillAmount = (Duration -_actualTime) / Duration;
            _actualTime += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator SendTimeUpdate()
    {
        yield return new WaitForSeconds(1);
        View.RPC("UpdateTime", RpcTarget.All, ActualTime);
    }

    [PunRPC]
    public void UpdateTime(float timeLeft)
    {
        ActualTime = timeLeft;
    }

    [PunRPC]
    public void GetDuration(float duration)
    {
        Duration = duration;
    }
}
