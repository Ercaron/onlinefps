﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class KillsCount : MonoBehaviourPun
{
    [SerializeField] Image img;
    [SerializeField] TextMeshProUGUI counter;
    int actualKills;
    void Start()
    {
        ExitGames.Client.Photon.Hashtable _customProp;
        _customProp = PhotonNetwork.LocalPlayer.CustomProperties;
        object kills;
        _customProp.TryGetValue(PhotonNetwork.LocalPlayer.NickName + "TotalKills",out kills);
        if (kills == null) kills = 0;
        counter.text = kills.ToString();
        actualKills = (int)kills;
    }

    [PunRPC]
    public void AddKill()
    {
        actualKills++;
        counter.text = actualKills.ToString();
    }
}
