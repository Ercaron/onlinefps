﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DemoSpells : MonoBehaviour
{
    [SerializeField] Color _col;
    [SerializeField] float _maxScale;
    [SerializeField] PhotonView _view;

    Renderer _mat;
    
    public Transform Follow { get; set; }
    
    
    void Start()
    {
        _mat = GetComponent<Renderer>();
        _view.GetComponent<PhotonView>();
        _mat.material.color = _col;
        StartCoroutine("ChangeScale");

        if (PhotonNetwork.IsMasterClient) Invoke("DestroyObject", 5f);
    }

    private void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        // _mat.material.color = _col;
        
        transform.position += -transform.up * 4 * Time.deltaTime;
        if (Follow)
            transform.position = new Vector3(Follow.position.x, transform.position.y, Follow.position.z);
    }

    IEnumerator ChangeScale()
    {
        while (transform.localScale.x < _maxScale)
        {
            transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            yield return new WaitForSeconds(0.05f);
        }
        _mat.enabled = false;
        
    }

    void DestroyObject()
    {
        PhotonNetwork.Destroy(this.gameObject);
    }
}
