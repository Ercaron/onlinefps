﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class WinnerFollow : MonoBehaviour
{
    [SerializeField] Vector3 offset;

    public Transform Follow { get; set; }

    void Start()
    {
        if (!PhotonNetwork.IsMasterClient) Destroy(this);        
    }

    void Update()
    {
        if(Follow)
            transform.position = Follow.position + offset;
    }
}
