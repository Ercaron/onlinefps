﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.IO;


public class SaveXML : MonoBehaviour
{
    public static SaveXML Instance;
    GameSettings _gameSettings;

    public GameSettings GameSettings { get => _gameSettings; set => _gameSettings = value; }

    private void Awake()
    {
        Instance = this;
        GameSettings = LoadData();
    }

    public void SaveGameSettings(Dictionary<string,KeyCode> dicKeys)
    {
        if (GameSettings != null) GameSettings = null;

        GameSettings = new GameSettings(dicKeys);
        //var loaded = LoadData();
        //if (loaded != null)
        //{
        //    gameData.EnemiesKilled += loaded.EnemiesKilled;
        //    gameData.TowersPurified += loaded.TowersPurified;
        //    gameData.Souls += loaded.Souls;
        //    if (upgrades == null) gameData.Upgrades = loaded.Upgrades;

        //}
        SaveData();

    }
    //WHEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    public void SaveGameSettings(GameSettings newData)  
    {
        GameSettings = newData;
        SaveData();
    }

    void SaveData()
    {
        //Debug.Log(Application.dataPath);
        DataContractSerializer serializer = new DataContractSerializer(typeof(GameSettings));
        //XmlSerializer serializer = new XmlSerializer(typeof(GameSettings));
        FileStream stream = new FileStream(Application.dataPath + "/Settings/GameSettings.xml", FileMode.Create);
        serializer.WriteObject(stream, GameSettings);
        stream.Close();
    }

    public GameSettings LoadData()
    {
        if (false == Directory.Exists(Application.dataPath + "/Settings/"))
        {
            Directory.CreateDirectory(Application.dataPath + "/Settings/");
            Debug.Log("No existe el Path broda");
            return null;
        }

        if (false == File.Exists(Application.dataPath + "/Settings/GameSettings.xml"))
        {
            Debug.Log("No existe el Archivo");
            return null;

        }

        //XmlSerializer serializer = new XmlSerializer(typeof(GameSettings));
        DataContractSerializer serializer = new DataContractSerializer(typeof(GameSettings));
        FileStream stream = new FileStream(Application.dataPath + "/Settings/GameSettings.xml", FileMode.Open);
        GameSettings data = (GameSettings)serializer.ReadObject(stream);
        stream.Close();
        return data;

    }
}

[System.Serializable]
public class GameSettings
{
    public Dictionary<string, KeyCode> Keys;

    //public int Souls;
    //public int EnemiesKilled;
    //public int TowersPurified;
    //public int[] Upgrades;

    public GameSettings(Dictionary<string,KeyCode> dicKeys)
    {
        Keys = dicKeys;
        //Souls = souls;
        //EnemiesKilled = enemies;
        //TowersPurified = towers;
        //if (upgrades == null) Upgrades = new int[] { 0, 0, 0, 0 };
        //else Upgrades = upgrades;

    }

    public GameSettings() { }

}

