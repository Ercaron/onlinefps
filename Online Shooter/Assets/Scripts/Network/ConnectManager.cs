﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;



public class ConnectManager : MonoBehaviourPunCallbacks
{
    [SerializeField] Button _connectButton;
    [SerializeField] Button _createRoomButton;
    [SerializeField] TMP_InputField _roomName;
    [SerializeField] GameObject _roomsListHolder;
    [SerializeField] GameObject _waitingText;
    [SerializeField] GameObject _backButton;
    [SerializeField] GameObject _lobbiesMenu;
    [SerializeField] GameObject _roomItemPrefab;
    [SerializeField] GameObject _errorHostStart;
    [SerializeField] GameObject _errorNotConnected;
    [SerializeField] List<Button> _loginButtons;
    Dictionary<string, GameObject> _roomsListUI;
    List<string> _previousRooms;



    void Start()
    {
       
        PhotonNetwork.AutomaticallySyncScene = true;

        PhotonNetwork.ConnectUsingSettings();
        _roomsListUI = new Dictionary<string, GameObject>();
        _previousRooms = new List<string>();

    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }   

    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.IsMasterClient) _connectButton.gameObject.SetActive(true);
        if (!PhotonNetwork.IsMasterClient) _waitingText.SetActive(true);
        _lobbiesMenu.SetActive(false);

        
        //_backButton.SetActive(true);
    }

  

    public override void OnJoinedLobby()
    {
        // _connectButton.interactable = true;
        //_createRoomButton.interactable = true;
        var options = new RoomOptions();
        options.MaxPlayers = 20;
        options.EmptyRoomTtl = 5000;

        //int roomsCount = PhotonNetwork.CountOfRooms;
        PhotonNetwork.JoinOrCreateRoom("MainServer", options, TypedLobby.Default);  
    }

    public override void OnLeftRoom()
    {
        _connectButton.gameObject.SetActive(false);
        _lobbiesMenu.SetActive(true);
        _waitingText.SetActive(false);
        _backButton.SetActive(false);
    }

    public override void OnLeftLobby()
    {
       // _connectButton.interactable = false;
        _createRoomButton.interactable = false;
    }


    public void CreateRoom()
    {
        if (PhotonNetwork.InRoom) { Debug.LogError("I was in Room " + PhotonNetwork.CurrentRoom.Name); PhotonNetwork.LeaveRoom();  }
        var options = new RoomOptions();
        options.MaxPlayers = 20;
        options.EmptyRoomTtl = 5000;

        //int roomsCount = PhotonNetwork.CountOfRooms;
        PhotonNetwork.CreateRoom(_roomName.text, options, TypedLobby.Default);
        
    }

    public void DisconnectFromRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

        
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        GameObject roomInfoItem = null;

        foreach (RoomInfo room in roomList)
        {

            if (_roomsListUI.ContainsKey(room.Name))
            {
                _roomsListUI.TryGetValue(room.Name, out roomInfoItem);
            }
            else
            {
                roomInfoItem =  Instantiate(_roomItemPrefab, _roomsListHolder.transform);
                _roomsListUI.Add(room.Name, roomInfoItem);
            }
            
            UpdateRoomInfoUI(roomInfoItem, room);


            if(PhotonNetwork.CountOfRooms == roomList.Count) //Solo hago la operacion de limpieza cuando el numero de rooms es el correcto
            {
                Debug.LogWarning("Try to remove from old list " + room.Name);
                if (_previousRooms.Contains(room.Name))
                {
                    Debug.LogWarning("Found it and removed " + room.Name);
                    _previousRooms.Remove(room.Name); //Remuevo los rooms existentes de la lista desde el ultimo refresh
                }
            }
        }

        if (PhotonNetwork.CountOfRooms == roomList.Count)
        {
            //Finalmente elimino los rooms viejos
            for (int i = 0; i < _previousRooms.Count; i++)//Solo me quedaron los rooms que ya no existen
            {
                Debug.LogWarning("Eliminating " + _previousRooms[i]);
                _roomsListUI.TryGetValue(_previousRooms[i], out roomInfoItem);
                Destroy(roomInfoItem);
                _roomsListUI.Remove(_previousRooms[i]);
            }

            _previousRooms.Clear();

            for (int i = 0; i < roomList.Count; i++)
            {
                _previousRooms.Add(roomList[i].Name);
            }
        }

    }

    void UpdateRoomInfoUI(GameObject roomInfoItem, RoomInfo info)//Should move to a UI component? 
    {
        if (!roomInfoItem)
        {
            Debug.LogError("Null Room Info");
            return;
        }

        var children = roomInfoItem.GetComponentsInChildren<TextMeshProUGUI>();
        foreach (TextMeshProUGUI text in children)
        {
            if (text.tag == "RoomName") text.text = info.Name;
            else text.text = info.PlayerCount.ToString() + "/" + info.MaxPlayers.ToString();

        }
    }

    public void Connect()
    {
        if (!PhotonNetwork.InRoom) { _errorNotConnected.SetActive(true); return; }

        if (PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("EnterGame", RpcTarget.All);
        }
        else _errorHostStart.SetActive(true);
    }

    [PunRPC]
    public void EnterGame()
    {
        PhotonNetwork.LoadLevel("Map_1");
    }

}
