﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Instantiator : MonoBehaviourPun
{
    [SerializeField] CharacterSelection _characterSelection;
    [SerializeField] InputField _nickname;

    GameObject _spawnedCharacter;
    public void Spawn()
    {

        if (_nickname.enabled == true &&_nickname.interactable)
        {
            PhotonNetwork.LocalPlayer.NickName = "Guest_" + _nickname.text;
        }


        if (_spawnedCharacter) PhotonNetwork.Destroy(_spawnedCharacter);

        //todo add random positions
        _spawnedCharacter = PhotonNetwork.Instantiate(_characterSelection.SelectedClass, new Vector3(-15, 2, 0), Quaternion.identity);
        //var character = _spawnedCharacter.GetComponentInChildren<Character>();

    }

}
