﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class GameServer : MonoBehaviourPunCallbacks
{

    [SerializeField] CharacterSelection _characterSelection;
    [SerializeField] InputField _nickname;
    [SerializeField] CharacterUI _characterUI;
    [SerializeField] SpellsUI _spells;
    [SerializeField] GameObject _playerCanvas;
    [SerializeField] WinManager _winManager;

    private static GameServer instance;

    Dictionary<Player, Character> _dic = new Dictionary<Player, Character>();
    Dictionary<Character, Player> _dicInverse = new Dictionary<Character, Player>();

    public Dictionary<Player, Character> Dic { get => _dic; }
    public Dictionary<Character, Player> DicInverse { get => _dicInverse; set => _dicInverse = value; }
    public static GameServer Instance { get => instance; set => instance = value; }




    private void Start()
    {
        if (instance) Destroy(this);
        else instance = this;
    }

    public void Spawn()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            string name = _nickname.text;
            if (_nickname.enabled == true && _nickname.interactable)
            {
                name = "Guest_" + _nickname.text;
                PhotonNetwork.LocalPlayer.NickName = name;
            }
            photonView.RPC("AddPlayer", PhotonNetwork.MasterClient, PhotonNetwork.LocalPlayer, _characterSelection.SelectedClass, name);

        }
        else
        {
            PhotonNetwork.LocalPlayer.NickName = "Server";
            _playerCanvas.SetActive(false);
        }
    }


    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Character chara;
        Dic.TryGetValue(otherPlayer, out chara);
        _winManager.RemovePlayer(chara.Name);
        PhotonNetwork.Destroy(Dic[otherPlayer].gameObject);
        Dic.Remove(otherPlayer);
        DicInverse.Remove(chara);
        


     
    }

    [PunRPC]
    void AddPlayer(Player client, string pathPrefab, string name)
    {

        Character previousCharacter = null;
        Dic.TryGetValue(client, out previousCharacter);

        if(previousCharacter != null) {
            _winManager.RemovePlayer(previousCharacter.Name);
            PhotonNetwork.Destroy(Dic[client].gameObject); 
        }


        GameObject obj = PhotonNetwork.Instantiate(pathPrefab, new Vector3(-15, 2, 0),Quaternion.identity);
        Character character = obj.GetComponent<Character>();
        int viewID = character.photonView.ViewID;
        if (character)
        {
            Dic[client] = character;
            DicInverse[character] = client;
            character.photonView.RPC("CameraFollow", client);
            character.photonView.RPC("Spawn", client);
            character.photonView.RPC("SetListener", client,client);
            photonView.RPC("SetController", client, viewID);
            photonView.RPC("SetName", RpcTarget.AllBuffered, viewID,name);
            photonView.RPC("SetCharacterOnWin", client, viewID);
            StartCoroutine(SetPlayerConfigurations(client, viewID));
            
        }
    }

    IEnumerator SetPlayerConfigurations(Player client, int viewID)
    {   
        yield return new WaitForSeconds(.5f);
        _characterUI.photonView.RPC("SetCharacter", client, viewID);
        _spells.photonView.RPC("SetCharacterSpells", client, viewID);
    }


    [PunRPC]
    void SetController(int viewID)
    {
        GameObject obj = PhotonView.Find(viewID).gameObject;
        CharacterController controller = obj.AddComponent<CharacterController>();
        controller.Char = obj.GetComponent<Character>(); 
    }

    [PunRPC]
    void SetName(int viewID,string name)
    {
        Character character = PhotonView.Find(viewID).gameObject.GetComponent<Character>();
        character.Name = name;
    }

    [PunRPC]
    void SetCharacterOnWin(int viewID)
    {
        Character character = PhotonView.Find(viewID).gameObject.GetComponent<Character>();
        _winManager.ClientName = character.Name;
    }

    
}
