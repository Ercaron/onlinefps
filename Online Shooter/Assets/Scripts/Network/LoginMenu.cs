﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;
using System;
using Photon.Pun;
using Photon.Realtime;

public class LoginMenu : MonoBehaviourPun
{
    [SerializeField] TMP_InputField _username; 
    [SerializeField] TMP_InputField _password;
    [SerializeField] Button _loginButton;
    [SerializeField] Button _createUserButton;

    [SerializeField] GameObject _gameUI;



    private void Awake()
    {
        Button[] btns = GetComponentsInChildren<Button>();
        foreach (var btn in btns)
        {
            btn.interactable = true;
        }
    }

    public void CreateUser()
    {
        if (PhotonNetwork.IsMasterClient)
            StartCoroutine(CreateUserDB(PhotonNetwork.LocalPlayer, _username.text, _password.text));
        else
            photonView.RPC("CreateUserRequest", PhotonNetwork.MasterClient, PhotonNetwork.LocalPlayer,_username.text,_password.text);
    }

    public void Login()
    {
        if(PhotonNetwork.IsMasterClient)
            StartCoroutine(LogInDB(PhotonNetwork.LocalPlayer, _username.text,_password.text));
        else
            photonView.RPC("LoginRequest", PhotonNetwork.MasterClient, PhotonNetwork.LocalPlayer, _username.text, _password.text);

    }


    [PunRPC]
    void CreateUserRequest(Player client, string username, string password)
    {
        StartCoroutine(CreateUserDB(client, username, password));
    }

    [PunRPC]
    void LoginRequest(Player client, string username, string password)
    {
        StartCoroutine(LogInDB(client, username, password));
    }

    [PunRPC]
    void CreateUserResponse(int result, string details)
    {
        if (result == 0)
        {
            Debug.Log("Created user succesfully"); //todo add text 
        }
        else if (result == 100)
        {
            Debug.LogError("Username already taken");//todo add error text
        }
    }

    [PunRPC]
    void LoginResponse(int result, string details, int kills)
    {
        if (result == 0)
        {
            Debug.Log("Logged in succesfully"); //todo add text 

           //Update Name
            PhotonNetwork.LocalPlayer.NickName = _username.text;

            //Update Kills
            ExitGames.Client.Photon.Hashtable _customProp;
            _customProp = PhotonNetwork.LocalPlayer.CustomProperties;
            _customProp[PhotonNetwork.LocalPlayer.NickName + "TotalKills"] = kills;
            PhotonNetwork.LocalPlayer.SetCustomProperties(_customProp);
            
            if(!PhotonNetwork.IsMasterClient) this.gameObject.SetActive(false);
            _gameUI.SetActive(true);
        }
        else if (result == 101)
        {
            Debug.LogError("Invalid Username or Password");//todo add error text
        }
        else if (result == 102)
        {
            Debug.LogError("User is already connected");
        }
    }

    IEnumerator CreateUserDB(Player client, string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        UnityWebRequest w = UnityWebRequest.Post(DBConnections.CREATE_USER, form);

        yield return w.SendWebRequest();

        if (w.isNetworkError)
        {
            if (client != PhotonNetwork.LocalPlayer)
                photonView.RPC("CreateUserResponse", client, -1, "Networking Error when trying to login");
            else
                Debug.LogError("Networking Error when creating the user");
        }
        else if (w.isHttpError)
        {
            if (client != PhotonNetwork.LocalPlayer)
                photonView.RPC("CreateUserResponse", client, -2, "Error 404 when creating the user");
            else
                Debug.LogError("Error 404 when creating the user");
        }
        else
        {
            print(w.downloadHandler.text);
            LoginData response = JsonUtility.FromJson<LoginData>(w.downloadHandler.text);

            if (client != PhotonNetwork.LocalPlayer)
                photonView.RPC("CreateUserResponse", client, response.error, response.description);
            else
                CreateUserResponse(response.error, response.description);
        }
    }

    IEnumerator LogInDB(Player client, string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        UnityWebRequest w = UnityWebRequest.Post(DBConnections.LOGIN, form);

        yield return w.SendWebRequest();

        if (w.isNetworkError)
        {
            if (client != PhotonNetwork.LocalPlayer)
                photonView.RPC("LoginResponse", client, -1, "Networking Error when trying to login",0);
            else
                Debug.LogError("Networking Error when trying to login");

        }
        else if (w.isHttpError)
        {
            if (client != PhotonNetwork.LocalPlayer)
                photonView.RPC("LoginResponse", client, -2, "Error 404 when trying to login", 0);
            else
                Debug.LogError("Error 404 when trying to login");

        }
        else
        {
            LoginData response = JsonUtility.FromJson<LoginData>(w.downloadHandler.text);

            if (client != PhotonNetwork.LocalPlayer)
                photonView.RPC("LoginResponse", client, response.error, response.description, response.kills);
            else
                LoginResponse(response.error,response.description,response.kills);
        }
    }

    
}
