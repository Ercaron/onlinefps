﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.Networking;
using System;

public class KillsManager : MonoBehaviour
{
    public static KillsManager Instance;
    [SerializeField] KillsCount _killsCount;
    ExitGames.Client.Photon.Hashtable _customProp;


    void Start()
    {
        if (Instance != null) Destroy(this);
        else Instance = this;
    }

    void Update()
    {

    }

    //public void AddPlayer(int playerID)
    //{
    //    if (!PhotonNetwork.IsMasterClient) return;

    //    PhotonView playerView = PhotonView.Find(playerID);


    //    _customProp = PhotonNetwork.CurrentRoom.CustomProperties;
    //    // _customProp[character.Name + "Kills"] = 0;

    //}

    public void UpdateKills(int playerID)
    {
        if (!PhotonNetwork.IsMasterClient || playerID == 0) return;

        PhotonView playerView = PhotonView.Find(playerID);
        Character character = playerView.GetComponent<Character>();


        _customProp = PhotonNetwork.CurrentRoom.CustomProperties;
        object kills;
        _customProp.TryGetValue(character.Name + character.Data.ClassName, out kills);
        
        if (kills == null) kills = 0;
        _customProp[character.Name + character.Data.ClassName] = (int)kills + 1;
        PhotonNetwork.CurrentRoom.SetCustomProperties(_customProp);
        
        //Debug.Log(_customProp[character.Name + character.Data.ClassName] + " son muchas kills "+character.Name );
        //object mageKills;
        //_customProp.TryGetValue(character.Name + "Mage", out mageKills);
        //if (mageKills == null) mageKills = 0;

        //object knightKills;
        //_customProp.TryGetValue(character.Name + "Knight", out knightKills);
        //if (knightKills == null) knightKills = 0;

        int mageKills = 0;
        int knightKills = 0;

        if (character.Data.ClassName == "Mage") mageKills = 1;
        else knightKills = 1;




        StartCoroutine(UpdateKillsRequest(character.Name,mageKills,knightKills));

        _killsCount.photonView.RPC("AddKill", GameServer.Instance.DicInverse[character]);
    }

    IEnumerator UpdateKillsRequest(string username, int mageKills, int knightKills)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("knightKills", knightKills);
        form.AddField("mageKills", mageKills);

        UnityWebRequest w = UnityWebRequest.Post(DBConnections.UPDATE_KILLS, form);

        yield return w.SendWebRequest();

        if (w.isNetworkError)
        {
            Debug.LogError("Networking Error when trying to update kills");
        }
        else if (w.isHttpError)
        {
            Debug.LogError("Error 404 when trying to update kills");
        }
    }
}
