﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Chat;
using ExitGames.Client.Photon;
using TMPro;
public class ChatManager : MonoBehaviour, IChatClientListener
{
    public TextMeshProUGUI content;
    public ScrollRect scroll;
    ChatClient _chatClient;
    public TMP_InputField inputField;
    public Button emptyBtn;

    public static ChatManager Instance;
    public PhotonView View { get; set; }

    bool connected;
    string _channel;
    bool _isPrivateMessage;
    int _count = 0;
    int _charCountLast = 0;
    bool selected;
    private void Start()
    {
        if (!PhotonNetwork.IsConnected) Destroy(this);
        if (Instance != null) Destroy(this);
        else Instance = this;

        View = GetComponent<PhotonView>();
    }

    public void ConnectToChat()
    {
        if (connected) return;
        _channel = PhotonNetwork.CurrentRoom.Name;
        _chatClient = new ChatClient(this);
        _chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat,
        PhotonNetwork.AppVersion,
         new AuthenticationValues(PhotonNetwork.NickName/* + "#" + PhotonNetwork.LocalPlayer.UserId*/));
        connected = true;
    }
    private void Update()
    {
        if (!connected) return;
        _chatClient.Service();
        if (Input.GetKeyDown(KeyCode.Return) && !selected)
        {
            inputField.Select();
            selected = true;
        }
        else if (Input.GetKeyDown(KeyCode.Return) && selected)
        {
            emptyBtn.Select();
            selected = false;
        }
        
    }
    
    public void SendMessages()
    {
        string messages = inputField.text;
        if (string.IsNullOrEmpty(messages) || string.IsNullOrWhiteSpace(messages)) return;

        string[] words = messages.Split(' ');
        if(words.Length > 1 && words[0][0] == '/')
        {
            
            string target = words[0].Replace("/",string.Empty);
            target = target.Replace("+", " ");
            bool exist = false;
            for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
            {
                if (PhotonNetwork.PlayerList[i].NickName.ToLower().Equals(target.ToLower()))
                {
                    exist = true;
                    target = PhotonNetwork.PlayerList[i].NickName;
                }
            }

            if(exist)
                _chatClient.SendPrivateMessage(target, string.Join(" ", words, 1, words.Length - 1));
        }
        else
        {
            _chatClient.PublishMessage(_channel, messages);
        }
        inputField.text = "";
    }
    public void DebugReturn(DebugLevel level, string message)
    {
    }

    public void OnChatStateChange(ChatState state)
    {
    }

    public void OnConnected()
    {
        _chatClient.Subscribe(_channel);
        inputField.gameObject.SetActive(true);
    }

    public void OnDisconnected()
    {
        print("Chat/Disconnected");
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        for (int i = 0; i < senders.Length; i++)
        {
            string color;
            if (senders[i] == "Server")
            {
                color = "<color=#fbff00>";
            }
            else if (senders[i] == PhotonNetwork.NickName)
            {
                color = "<color=#008080ff>"; 
            }
            else
                color = "<color=#add8e6ff>";

            string newString = content.text;
            newString += "\n" + "<b>[" + color + senders[i] + "</color>" + "]: </b>" + messages[i];



            content.text = LimitMessagesCount(newString);

        }
        CheckAutoScroll();
    }

    string LimitMessagesCount(string originalMessages)
    {
        string[] splitText = originalMessages.Split(new string[] { "\n" },  System.StringSplitOptions.None);

        string completeText = "";
        if(splitText.Length > 20)
        {
            string[] messages = new string[20];
            for (int i = 0; i < messages.Length; i++)
            {
                messages[(messages.Length - 1) - i] = splitText[(splitText.Length - 1) - i];
            }
            
            for (int i = 0; i < 20; i++)
            {
                completeText = string.Join("\n", messages);
            }

            return completeText;
        }

        return originalMessages;
    }

    void CheckAutoScroll()
    {
        if (scroll.verticalNormalizedPosition < 0.2f)
        {
            StartCoroutine(WaitToScroll());
        }
    }
    IEnumerator WaitToScroll()
    {
        yield return new WaitForEndOfFrame();
        scroll.verticalNormalizedPosition = 0;
    }
    public void OnPrivateMessage(string sender, object message, string channelName)
    {
       
        content.text += "\n" +  "<i><color=orange>" + "<b>[" + sender + "]: </b> " + message + "</color> </i>";
        CheckAutoScroll();

    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        string action = "";
        if (status == 2)
        {
            action = "se conecto";
        }
        else if (status == 0)
        {
            action = "se desconecto";
        }
        content.text += "\n" + "<color=red>" + user + ": " + action + "</color>";
        CheckAutoScroll();
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {

    }

    public void OnUnsubscribed(string[] channels)
    {
    }

    public void OnUserSubscribed(string channel, string user)
    {

    }

    public void OnUserUnsubscribed(string channel, string user)
    {
    }

    [PunRPC]
    public void WriteOnConsole(string msg)
    {
        content.text += "\n" + msg;
        CheckAutoScroll();
    }
}
