﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class LobbiesButton : MonoBehaviourPun
{
    [SerializeField] TextMeshProUGUI _roomName;

    int _counter = 2;
    float _timeWait;
    bool processing;

    public void JoinRoom()
    {
        if (_counter == 2)
        {
            _counter--;
            Invoke("RestartCounter", 2f);
            return;
        }       

        if (PhotonNetwork.InRoom && PhotonNetwork.CurrentRoom.Name == _roomName.text) return;
        if (processing) return;
        if (PhotonNetwork.InRoom && PhotonNetwork.CurrentRoom.Name != _roomName.text)
        {
            PhotonNetwork.LeaveRoom();
            _timeWait = 2f;
        }
        else _timeWait = 0;


        StartCoroutine("Connect");
    }

    IEnumerator Connect()
    {
        processing = true;
        yield return new WaitForSeconds(_timeWait);
        PhotonNetwork.JoinRoom(_roomName.text);

        yield return new WaitForSeconds(2f);
        processing = false;
    }

    void RestartCounter()
    {
        _counter = 2;
        CancelInvoke();
    }
}
