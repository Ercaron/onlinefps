﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DBConnections
{
    public const string CREATE_USER = "http://localhost/argentumonline/createUser.php";
    public const string LOGIN = "http://localhost/argentumonline/login.php";
    public const string UPDATE_KILLS = "http://localhost/argentumonline/updateKills.php";
}
