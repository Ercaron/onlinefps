﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HitComponent : MonoBehaviour
{

    [SerializeField] HitCollider _hitBox;
    
    Character _character;
    PhotonView _view;
    float _lastHit;
    float _hitCooldown;
    

    public float HitCooldown { get => _hitCooldown; set => _hitCooldown = value; }
    public Character Character { get => _character; 
        set 
        {
            _character = value;
            HitCooldown = _character.Data.MeleeCooldown;
        } 
    }

    private void Start()
    {
        _view = GetComponent<PhotonView>();
        if (!_view.IsMine && !PhotonNetwork.IsMasterClient) Destroy(this);

        _hitBox.OnHit += HitEnemy;
    }

    public IEnumerator Hit()
    {
        if(Time.time > _lastHit + HitCooldown)
        {
            _view.RPC("HitAnim", RpcTarget.AllBuffered, true);
            _lastHit = Time.time;
            _hitBox.gameObject.SetActive(true);
            yield return new WaitForSeconds(.2f);
            _hitBox.gameObject.SetActive(false);
            if(!_hitBox.EnemyHit) SoundManager.Instance.View.RPC("PlaySound", RpcTarget.All, _character.photonView.ViewID, "MeleeMiss");
            yield return new WaitForSeconds(1f);
            _view.RPC("HitAnim", RpcTarget.AllBuffered, false);
        }
        
    }

    public void HitEnemy(Character target)
    {
        if (!target.Alive) return;
        int damage = Random.Range(_character.Data.MeleeMinDamage, _character.Data.MeleeMaxDamage);
        target.HealthComponent.Damage(damage,target.photonView.ViewID,_character.photonView.ViewID);
        SoundManager.Instance.View.RPC("PlaySound", RpcTarget.All, _character.photonView.ViewID, "MeleeHit");
    }


}
