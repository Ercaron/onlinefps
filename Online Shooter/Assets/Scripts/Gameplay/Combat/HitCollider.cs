﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HitCollider : MonoBehaviour
{
    public delegate void Hit(Character target);
    public event Hit OnHit;



    [SerializeField] Character _character;

    int _targetsCount = 0;
    public bool EnemyHit;

    private void OnEnable()
    {
        _targetsCount = 0;
        EnemyHit = false;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (_targetsCount == 0)
        {
            Character otherChar = other.GetComponent<Character>();
            if (otherChar != null && otherChar != _character)
            {
                _targetsCount++;
                EnemyHit = true;
                OnHit(otherChar);
            }
        }
    }
}
