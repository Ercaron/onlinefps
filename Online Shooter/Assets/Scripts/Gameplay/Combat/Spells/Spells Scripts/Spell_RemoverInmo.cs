﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_RemoverInmo : Spell
{
    public override int Cast(ISpellable caster, ISpellable target)
    {
        if (caster.ManaComponent.CurrentMana >= Data.ManaCost)
        {
            //todo move to all spells

            if (!target.CanMove)
            {
                ApplySpell(caster, target);
                caster.ManaComponent.UseMana(Data.ManaCost);
                return 0;
            }

            
        }
      
        return -1;
        
    }

    public override int ApplySpell(ISpellable caster, ISpellable target)
    {
        target.CanMove = Data.CanMove;
        return 0;
    }
}
