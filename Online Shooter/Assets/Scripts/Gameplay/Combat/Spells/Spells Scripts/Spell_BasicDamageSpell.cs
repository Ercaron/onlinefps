﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_BasicDamageSpell : Spell
{
    
    public override int Cast(ISpellable caster, ISpellable target)
    {
        if (caster.ManaComponent.CurrentMana >= Data.ManaCost)
        {
            int dmg = ApplySpell(caster, target);
            caster.ManaComponent.UseMana(Data.ManaCost);
            return dmg;
        }

        return -1;
    }

    public override int ApplySpell(ISpellable caster, ISpellable target)
    {        
        int dmg = Random.Range(Data.MinDamage, Data.MaxDamage);
        target.HealthComponent.Damage(dmg,target.ViewID,caster.ViewID);

        return dmg;
    }
}
