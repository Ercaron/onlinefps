﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class SpellsHandler : MonoBehaviourPun
{
    public static SpellsHandler Instance;


    [SerializeField] List<SpellData> _spellsData;

    List<Spell> _spells;
    Dictionary<string, Spell> _spellsByName;

    public List<Spell> Spells { get => _spells; }
    public Dictionary<string, Spell> SpellsByName { get => _spellsByName; }


    void Awake()
    {

        if (Instance != null) { Destroy(this); }
        else Instance = this;

        _spells = new List<Spell>();
        _spellsByName = new Dictionary<string, Spell>();

        for (int i = 0; i < _spellsData.Count; i++)
        {
            Type t = Type.GetType(_spellsData[i].ClassName);
            Spell spell = (Spell)Activator.CreateInstance(t);
            spell.Data = _spellsData[i];
            Spells.Add(spell);
            SpellsByName.Add(_spellsData[i].SpellName, spell);
        }
    }


    [PunRPC]
    public void CastedSpell(string spell, int damage, string target)
    {
        Spell sp;
        SpellsByName.TryGetValue(spell, out sp);

        string casterMsg;
        if (damage > 0)
            casterMsg = "<color=red>" + "Lanzaste " + sp.Data.SpellName + " Sobre " + target + " por " + damage + " de daño." + "</color>";
        else
            casterMsg = "<color=red>" + "Lanzaste " + sp.Data.SpellName + " Sobre " + target + "." + "</color>";

        ChatManager.Instance.WriteOnConsole(casterMsg);


    }

    [PunRPC]
    public void ReceivedSpell(string spell,int damage, int casterId, int targetId, string casterName)
    {
        
        PhotonView casterView = PhotonView.Find(casterId);
        PhotonView targetView = PhotonView.Find(targetId);

        ISpellable caster = null;
        if (casterView)
            caster = casterView.GetComponent<ISpellable>();

        ISpellable target = null; 
        if(targetView)
            target = targetView.GetComponent<ISpellable>();

        Spell spellToCast;
        SpellsByName.TryGetValue(spell, out spellToCast);

        string affectedMsg;
        if (damage > 0)
            affectedMsg ="<color=red>" + casterName + " ha lanzado " + spellToCast.Data.SpellName + " Sobre " + " ti por " + damage + " de daño." + "</color>";
        else
            affectedMsg = "<color=red>" + casterName + " ha lanzado " + spellToCast.Data.SpellName + " Sobre " + " ti." + "</color>";

        ChatManager.Instance.WriteOnConsole(affectedMsg);


        if (PhotonNetwork.IsMasterClient) return;

        spellToCast.ApplySpell(caster, target);




    }

    [PunRPC]
    public void RemoveSpellEffect(string spell, int targetId)
    {
        PhotonView targetView = PhotonView.Find(targetId);

        ISpellable target = targetView.GetComponent<ISpellable>();

        Spell spellToCast;
        SpellsByName.TryGetValue(spell, out spellToCast);

        if (PhotonNetwork.IsMasterClient) return;

        spellToCast.RemoveSpellEffect(target);
    }

    
}
