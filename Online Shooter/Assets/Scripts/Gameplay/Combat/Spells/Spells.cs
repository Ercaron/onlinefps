﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Spells : MonoBehaviour
{
    [SerializeField] List<Spell> _spells;
    [SerializeField] SpellsUI _spellsUI;
     Character _character;
    Dictionary<string, Spell> _spellsAndNames;
    Dictionary<string, float> _activeSpells;
    Dictionary<string, GameObject> _spellTimers; 

    public delegate void AddedSpell(int index);
    public event AddedSpell OnAddedSpell;

    public List<Spell> SpellsList { get => _spells; set => _spells = value; }
    public Character Character { get => _character; }
    public Dictionary<string, Spell> SpellsAndNames { get => _spellsAndNames; set => _spellsAndNames = value; }
    public Dictionary<string, float> ActiveSpells { get => _activeSpells; set => _activeSpells = value; }
    public Dictionary<string, GameObject> SpellTimers { get => _spellTimers; set => _spellTimers = value; }

    private void Awake()
    {
        SpellsList = new List<Spell>();
        _spellsAndNames = new Dictionary<string, Spell>();
        _character = GetComponent<Character>();
        _activeSpells = new Dictionary<string, float>();
        _spellTimers = new Dictionary<string, GameObject>();
    }


    private void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        if(_activeSpells.Count > 0)
        {
            List<Spell> removeSpells = new List<Spell>();
            foreach (var spell in _activeSpells)
            {
                Spell spellToRemove;
                SpellsAndNames.TryGetValue(spell.Key, out spellToRemove);

                if (Time.time >= spellToRemove.Data.Duration + spell.Value)
                {
                    removeSpells.Add(spellToRemove);
                    RemoveActiveSpell(spellToRemove);
                }
            }

            for (int i = 0; i < removeSpells.Count; i++)
            {
                _activeSpells.Remove(removeSpells[i].Data.SpellName);
            }
        }
    }

    public void RemoveActiveSpell(Spell spell)
    {
        spell.RemoveSpellEffect(_character);
        SpellsHandler.Instance.photonView.RPC("RemoveSpellEffect", _character.photonView.Owner, spell.Data.SpellName, _character.photonView.ViewID);
        GameObject go;
        SpellTimers.TryGetValue(spell.Data.SpellName, out go);
        SpellTimers.Remove(spell.Data.SpellName);
        PhotonNetwork.Destroy(go);
    }

    public void AddSpell(SpellData spell)
    {
        Type t = Type.GetType(spell.ClassName);
        Spell ob =  (Spell) Activator.CreateInstance(t);
        ob.Data = spell;
        _spells.Add(ob);
        _spellsAndNames.Add(spell.SpellName, ob);

        //OnAddedSpell(_spells.Count-1);
    }


    [PunRPC]
    public void CastSpell(string spell, int casterId, int targetId)
    {
        

        PhotonView casterView = PhotonView.Find(casterId);
        PhotonView targetView = PhotonView.Find(targetId);
        Character caster = casterView.GetComponent<Character>();
        Character target = targetView.GetComponent<Character>();

        if (!caster.Alive) //todo Move to local player
        {
            Debug.LogError("You are Dead! You can't cast spells");
            return;
        }

        if (!target.Alive)
        {
            Debug.LogError("The target is already Dead! You can't cast spells on a Ghost");
            return;
        }

        Spell spellToCast;
        SpellsAndNames.TryGetValue(spell, out spellToCast);
        int castedSpellDamage = spellToCast.Cast(caster, target);

        if (castedSpellDamage >= 0)
        {
            SpellTravel spellTravel = PhotonNetwork.Instantiate("Spells/SpellTravel", caster.transform.position, Quaternion.identity).GetComponent<SpellTravel>();
            spellTravel.ObjectToFollow = target.transform;
            

            DemoSpells go = PhotonNetwork.Instantiate(spellToCast.Data.SpellObjectPath,targetView.gameObject.transform.position+new Vector3(0,2,0),Quaternion.identity).GetComponent<DemoSpells>();
            go.Follow = targetView.transform;

            SpellsHandler.Instance.photonView.RPC("CastedSpell", GameServer.Instance.DicInverse[caster], spell, castedSpellDamage, target.Name);
            SpellsHandler.Instance.photonView.RPC("ReceivedSpell", GameServer.Instance.DicInverse[target], spell,castedSpellDamage, casterId, targetId, caster.Name);


            if (spellToCast.Data.Duration > 0)
            {
                if (!target.Spells.ActiveSpells.ContainsKey(spell))
                {
                    target.Spells.ActiveSpells.Add(spell, Time.time);

                   

                    SpellDurationUI spellTimer = PhotonNetwork.Instantiate(spellToCast.Data.SpellTimerPath, targetView.gameObject.transform.position, Quaternion.identity).GetComponent<SpellDurationUI>();

                    spellTimer.Follow = targetView.transform;
                    spellTimer.Offset = Vector3.back;
                    spellTimer.View.RPC("GetDuration", RpcTarget.AllBuffered, spellToCast.Data.Duration);
                    target.Spells.SpellTimers.Add(spell, spellTimer.gameObject);
                }
            }


            if (spellToCast.Data.RemoveSpell != null && spellToCast.Data.RemoveSpell.Length > 0)
            {
                Spell spellToRemove;
                SpellsAndNames.TryGetValue(spellToCast.Data.RemoveSpell, out spellToRemove);
                if (target.Spells.ActiveSpells.ContainsKey(spellToCast.Data.RemoveSpell))
                {
                    target.Spells.RemoveActiveSpell(spellToRemove);
                    target.Spells.ActiveSpells.Remove(spellToRemove.Data.SpellName);
                }

            }
        }
       
    }

    
}
