﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellUI : MonoBehaviour
{
    Button _btn;
    Spell _spell;
    Character _caster;

    public Spell Spell { get => _spell; set => _spell = value; }
    public Character Caster { get => _caster; set => _caster = value; }

    void Awake()
    {
        _btn = GetComponent<Button>();
        _btn.onClick.AddListener(SelectSpell);
    }

   //todo add cooldown

    void SelectSpell()
    {
        Caster.SelectedSpell = Spell;
    }
}
