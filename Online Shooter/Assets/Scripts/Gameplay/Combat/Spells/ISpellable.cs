﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpellable
{
    Health HealthComponent { get; set; }
    Mana ManaComponent { get; set; }
    bool CanMove { get; set; }
    bool Alive { get; set; }

    Spells Spells { get; set; }
    
    int ViewID { get; set; }
}
