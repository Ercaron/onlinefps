﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AO/Combat/Spell")]
public class SpellData : ScriptableObject
{
    [SerializeField] string spellName;
    [SerializeField] int minDamage;
    [SerializeField] int maxDamage;
    [SerializeField] int manaCost;
    [SerializeField] bool canMove = true;
    [SerializeField] float duration;
    [SerializeField] string removeSpell;
    [SerializeField] Sprite icon;
    [SerializeField] string spellObjectPath;
    [SerializeField] string spellTimerPath;
    [SerializeField] string className;

    public string SpellName { get => spellName; set => spellName = value; }
    public int MinDamage { get => minDamage; set => minDamage = value; }
    public int MaxDamage { get => maxDamage; set => maxDamage = value; }
    public int ManaCost { get => manaCost; set => manaCost = value; }
    public Sprite Icon { get => icon; set => icon = value; }
    public string SpellObjectPath { get => spellObjectPath; set => spellObjectPath = value; }
    public bool CanMove { get => canMove; set => canMove = value; }
    public string ClassName { get => className; set => className = value; }
    public float Duration { get => duration; set => duration = value; }
    public string RemoveSpell { get => removeSpell; set => removeSpell = value; }
    public string SpellTimerPath { get => spellTimerPath; set => spellTimerPath = value; }
}
