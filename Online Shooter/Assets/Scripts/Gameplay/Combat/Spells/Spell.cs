﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Spell
{
    [SerializeField] SpellData _data;

    public SpellData Data { get => _data; set => _data = value; }

    public virtual int Cast(ISpellable caster, ISpellable target)
    {
        Debug.LogError("Casteo del padre");
        return 0;
    }

    public virtual int ApplySpell(ISpellable caster, ISpellable target)
    {
        return 0;
    }

    public virtual void RemoveSpellEffect(ISpellable target)
    {

    }


    [PunRPC]
    public virtual void CastedSpell(string spell, int damage, string target)
    {
    }

    [PunRPC]
    public virtual void ReceivedSpell()
    {

    }
}
