﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpellTravel : MonoBehaviour
{
    [SerializeField] float _speed;
    [SerializeField] Vector3 offset;
    public Transform ObjectToFollow { get; set; }

    void Start()
    {
        if (!PhotonNetwork.IsMasterClient) Destroy(this);
    }
    void Update()
    {
        if (!ObjectToFollow) return;

        
        transform.position = Vector3.MoveTowards(transform.position, ObjectToFollow.position, _speed);
        transform.position = new Vector3(transform.position.x, 2, transform.position.z);

        if(Vector3.Distance(transform.position+offset, ObjectToFollow.position+offset) <= 2)
        {
            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}
