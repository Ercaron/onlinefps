﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class SpellsUI : MonoBehaviourPun
{
    [SerializeField] Spells _spells;
    List<Button> _spellButtons;


    void Awake()
    {
        _spellButtons = new List<Button>();
    }

    

    public void UpdateSpellUI(int index)
    {
        _spellButtons[index].image.sprite = _spells.SpellsList[index].Data.Icon;
        SpellUI spellUI = _spellButtons[index].GetComponent<SpellUI>();
        spellUI.Spell = _spells.SpellsList[index];
        spellUI.Caster = _spells.Character;
        _spellButtons[index].GetComponentInChildren<Text>().text = _spells.SpellsList[index].Data.SpellName;

        //todo mana cost
    }

    [PunRPC]
    public void SetCharacterSpells(int viewID)
    {
        if(_spells != null)
        {
            _spells.OnAddedSpell -= UpdateSpellUI;
        }

        Character character = PhotonView.Find(viewID).gameObject.GetComponent<Character>();

        _spells = character.Spells;
       // _spells.OnAddedSpell += UpdateSpellUI;

        _spellButtons.Clear();

        var btns = GetComponentsInChildren<Button>(true);
        for (int i = 0; i < _spells.SpellsList.Count; i++)
        {
            _spellButtons.Add(btns[i]);
            UpdateSpellUI(i);
        }
        if (_spells.SpellsList.Count < btns.Length) btns[btns.Length - 1].gameObject.SetActive(false);
        else btns[btns.Length - 1].gameObject.SetActive(true);

    }
}
