﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Keybinds : MonoBehaviour
{
    public delegate void SettingsUpdated();
    public event SettingsUpdated OnSettingsUpdated;

    [SerializeField] Button _saveBtn;
    [SerializeField] Button _closeBtn;
    [SerializeField] GameObject _keybindsMenu;
    [SerializeField] List<Keybind> _keybinds;
    public static Keybinds _instance;

    Keybind _selectedKey;
    List<Keybind> _changedKeybinds = new List<Keybind>();

    public static Keybinds Instance { get => _instance; set => _instance = value; }
    public Dictionary<string, KeyCode> DicKeybinds { get => _dicKeybinds; set => _dicKeybinds = value; }
    public Dictionary<KeyCode, string> DicInvKeybinds { get => _dicInvKeybinds; set => _dicInvKeybinds = value; }

    Dictionary<string, KeyCode> _dicKeybinds = new Dictionary<string, KeyCode>();
    Dictionary<string, KeyCode> _dicSave;
    Dictionary<KeyCode, string> _dicInvKeybinds = new Dictionary<KeyCode, string>();
    Dictionary<KeyCode, string> _dicSaveInv = new Dictionary<KeyCode, string>();
    Dictionary<string, Button> _dicKeysBtns = new Dictionary<string, Button>();



    private void Awake()
    {
        DicKeybinds.Add("up", KeyCode.W);
        DicKeybinds.Add("down", KeyCode.S);
        DicKeybinds.Add("left", KeyCode.A);
        DicKeybinds.Add("right", KeyCode.D);
        DicKeybinds.Add("attack", KeyCode.LeftShift);
        DicKeybinds.Add("hppotion", KeyCode.LeftAlt);
        DicKeybinds.Add("manapotion", KeyCode.Space);
        DicKeybinds.Add("respawn", KeyCode.P);
        DicKeybinds.Add("dance", KeyCode.Y);
        DicKeybinds.Add("voice", KeyCode.V);


        DicInvKeybinds.Add(KeyCode.W, "up");
        DicInvKeybinds.Add(KeyCode.S, "down");
        DicInvKeybinds.Add(KeyCode.A, "left");
        DicInvKeybinds.Add(KeyCode.D, "right");
        DicInvKeybinds.Add(KeyCode.LeftShift, "attack");
        DicInvKeybinds.Add(KeyCode.LeftAlt, "hppotion");
        DicInvKeybinds.Add(KeyCode.Space, "manapotion");
        DicInvKeybinds.Add(KeyCode.P, "respawn");
        DicInvKeybinds.Add(KeyCode.Y, "dance");
        DicInvKeybinds.Add(KeyCode.V, "voice");
    }
    void Start()
    {
        if (!_instance) _instance = this;
        if (Instance != this) Destroy(gameObject);
        _saveBtn.onClick.AddListener(SaveKeybindings);
        _closeBtn.onClick.AddListener(CloseSettings);

        GameSettings loadedSettings = SaveXML.Instance.GameSettings;

        if (loadedSettings != null)
        {
            foreach (var item in loadedSettings.Keys)
            {
                DicKeybinds[item.Key] = item.Value;
                DicInvKeybinds[item.Value] = item.Key;
            }
        }
        else SaveXML.Instance.SaveGameSettings(_dicSave);


        foreach (var keybindUI in _keybinds)
        {
            keybindUI.KeyText.text = DicKeybinds[keybindUI.ActionName].ToString();
            keybindUI.Button.onClick.AddListener(delegate { KeySelected(keybindUI);});
        }
        _dicSave = new Dictionary<string, KeyCode>(DicKeybinds);
        _dicSaveInv = new Dictionary<KeyCode, string>(DicInvKeybinds);

    }


    void KeySelected(Keybind keybind)
    {
        _selectedKey = keybind;
    }

    void OnGUI()
    {
        if (_selectedKey)
        {
            Event e = Event.current;
            if (e.isKey)
            {
                if ((DicInvKeybinds.ContainsKey(e.keyCode) && _dicSaveInv.ContainsKey(e.keyCode))
                    || _dicSaveInv.ContainsKey(e.keyCode))
                {
                    //Show message not repeat.
                    Debug.LogError("You cannot repeat the keys");
                    _selectedKey = null;
                    return;
                }

                _selectedKey.KeyText.text = e.keyCode.ToString();
                _dicSave[_selectedKey.ActionName] = e.keyCode;
                _dicSaveInv.Remove(_dicKeybinds[_selectedKey.ActionName]);
                _dicSaveInv.Add(e.keyCode, _selectedKey.ActionName);

                _changedKeybinds.Add(_selectedKey);

                _selectedKey = null;
            }
        }
    }

    void SaveKeybindings()
    {
        SaveXML.Instance.SaveGameSettings(_dicSave);
        _dicKeybinds = new Dictionary<string, KeyCode>(_dicSave);
        _dicInvKeybinds = new Dictionary<KeyCode, string>(_dicSaveInv);

        _changedKeybinds.Clear();
        OnSettingsUpdated();
    }

    void CloseSettings()
    {
        if(_changedKeybinds.Count > 0)
        {
            foreach (var keybindUI in _changedKeybinds)
            {
                keybindUI.KeyText.text = _dicKeybinds[keybindUI.ActionName].ToString();
            }
        }

        _dicSave = new Dictionary<string, KeyCode>(DicKeybinds);
        _dicSaveInv = new Dictionary<KeyCode, string>(DicInvKeybinds);

        _changedKeybinds.Clear();

        _keybindsMenu.SetActive(false);
    }
}
