﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SoundManager : MonoBehaviour
{

    public static SoundManager Instance;


    [SerializeField] List<Sound> _sounds;

    PhotonView _view;
    Dictionary<string, AudioClip> _nameSound = new Dictionary<string, AudioClip>();

    public PhotonView View { get => _view; set => _view = value; }

    private void Awake()
    {
        if (Instance != null) { Destroy(this); }
        else Instance = this;

        View = GetComponent<PhotonView>();

        for (int i = 0; i < _sounds.Count; i++)
        {
            _nameSound.Add(_sounds[i].name, _sounds[i].clip); 
        }

    }



    [PunRPC]
    public void PlaySound(int callerId,string sound)
    {
        PhotonView callerView = PhotonView.Find(callerId);
        AudioSource audioPlayer = callerView.gameObject.GetComponent<AudioSource>();
        AudioClip clip;
        _nameSound.TryGetValue(sound, out clip);
        if (clip)
            audioPlayer.PlayOneShot(clip);

    }
    
}

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;
}
