﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UIElements;

public class Character : MonoBehaviourPun, ISpellable
{
  
    [SerializeField] CharacterData _data;
    [SerializeField] GameObject _aliveBody;
    [SerializeField] GameObject _deadBody;
    [SerializeField] AudioSource _potionSound;
    [SerializeField] Texture2D _cursor; //todo move to a Cursor Manager singleton
    [SerializeField] GameObject camaraFollow;
    [SerializeField] Sound_UI sound_UI;

    Rigidbody _rb;
    Animator _anim;
    AudioListener listener;
    Health _healthComponent;
    Mana _manaComponent;
    Spells _spellsComponent;
    HitComponent _hitComponent;
    List<Spells> _activeSpells;
    string _name;
   

    Spell _selectedSpell;
    bool _canMove = true;
    bool _alive = true;
    int _clickCount = 1;
    float _potionInterval = .5f;
    float _lastPotion;
    int _viewID;
    Vector3 _moveVector = Vector3.zero;

    public Health HealthComponent { get => _healthComponent; set => _healthComponent = value; }
    public bool CanMove { get => _canMove; set => _canMove = value; }
    public Mana ManaComponent { get => _manaComponent; set => _manaComponent = value; }
    public CharacterData Data { get => _data; set => _data = value; }
    public Spell SelectedSpell { get => _selectedSpell; 
        set 
        {
            _selectedSpell = value;
            if (_selectedSpell != null)
            {
                _clickCount = 1;
                UnityEngine.Cursor.SetCursor(_cursor, new Vector2(32, 24), CursorMode.Auto);
            }
            else UnityEngine.Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        } 
    }

    public bool Alive { get => _alive; set => _alive = value; }
    public Spells Spells { get => _spellsComponent;set => _spellsComponent = value; }
    public int ViewID { get => _viewID; set => _viewID = value; }
    public string Name { get => _name; set => _name = value; }

    private void Awake()
    {
        //todo solo hacer si es mio o el master
        _rb = GetComponentInChildren<Rigidbody>();
        _anim = GetComponentInChildren<Animator>();
        listener = GetComponentInChildren<AudioListener>(true);
        HealthComponent = GetComponentInChildren<Health>();
        ManaComponent = GetComponentInChildren<Mana>();
        _spellsComponent = GetComponentInChildren<Spells>();
        _hitComponent = GetComponentInChildren<HitComponent>();

    }

    void Start()
    {
        //todo solo hacer si es mio o el master
        HealthComponent.MaxHP = _data.MaxHealth;
        HealthComponent.Character = this;
        ManaComponent.MaxMana = _data.MaxMana;
        ManaComponent.Character = this;
        _hitComponent.Character = this;
        _viewID = photonView.ViewID;

        HealthComponent.onDeath += Die;

        for (int i = 0; i < _data.Spells.Count; i++)
        {
            _spellsComponent.AddSpell(_data.Spells[i]);
            //todo update UI
        }


        if (PhotonNetwork.IsMasterClient) WinManager.Instance.AddPlayer(photonView.ViewID);

    }

    void Update()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            Move();
        }
    }

    [PunRPC]
    public void CameraFollow()
    {
        camaraFollow.SetActive(true);
    }

    [PunRPC]
    public void Spawn()
    {
        SoundManager.Instance.View.RPC("PlaySound", RpcTarget.All, photonView.ViewID, "Spawn");
    }

    //todo
    public void Dance()
    {
        _anim.SetBool("dance", !_anim.GetBool("dance"));
    }

    [PunRPC]
    public void RequestMove(float x, float y)
    {
        _moveVector = new Vector3(x, 0, y);

    }
    void Move()
    {
        if ((_moveVector.x != 0 || _moveVector.z != 0))
        {
            if ((Alive && CanMove) || !Alive)
            {
                var move = _moveVector;
                move *= _data.Speed;
                move.y = _rb.velocity.y;
                _rb.velocity = move;
                move.y = 0;
                transform.forward = move.normalized;
                _anim.SetFloat("speed", _data.Speed);
                _anim.SetBool("dance", false);
                return;
            }

        }

        _anim.SetFloat("speed", 0);
    
    }

    public void RequestHit()
    {
        photonView.RPC("Hit", RpcTarget.MasterClient);
        
    }


    [PunRPC]
    public void Hit()
    {
        if(Alive)
        _hitComponent.StartCoroutine("Hit");
    }

    [PunRPC]
    public void HitAnim(bool started)
    {
        _anim.SetBool("attack", started);
        if(started) _anim.SetLayerWeight(1, 1);
        else _anim.SetLayerWeight(1, 0);
    }

    

    
    public void TryCastSpell()
    {
        if (_selectedSpell == null || !Alive) {SelectedSpell = null; return; }
        if (_clickCount == 1) { _clickCount--; return; }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            ISpellable target = hit.collider.gameObject.GetComponent<ISpellable>();
            if (target != null)
            {
                PhotonView targetView = hit.collider.gameObject.GetComponentInParent<PhotonView>();
                photonView.RPC("CastSpell", PhotonNetwork.MasterClient, _selectedSpell.Data.SpellName, photonView.ViewID, targetView.ViewID);
            }
        }

        _clickCount = 1;
        SelectedSpell = null;
        //todo generic get target from spell (position or what)
    }

    public void RequestUsePotion(string potionType)
    {
        if (Alive) photonView.RPC("UsePotion", PhotonNetwork.MasterClient, potionType);
        else Debug.Log("You are dead!");
    }

    [PunRPC]
    public void UsePotion(string potionType)
    {
        if(Time.time > _lastPotion + _potionInterval && Alive)//how to add attack interval
        {
            if (potionType.Equals("red"))
            {
                _healthComponent.Heal(25);
                //photonView.RPC("UpdateHP", photonView.Owner, _healthComponent.CurrentHP);
            }
            else if (potionType.Equals("blue"))
            {
                _manaComponent.GainMana((int)(_manaComponent.MaxMana * 0.05));
                //photonView.RPC("UpdateMana", photonView.Owner, _manaComponent.CurrentMana);
            }

            SoundManager.Instance.View.RPC("PlaySound", RpcTarget.All, photonView.ViewID, "Potion");
        }
    }

  

    [PunRPC]
    public void UpdateHP(int currentHP)
    {
        HealthComponent.UpdateHP(currentHP);
    }

    [PunRPC]
    public void UpdateMana(int currentMana)
    {
        ManaComponent.UpdateMana(currentMana);
    }
    public void Die(int killedID, int killerID)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if(killedID != killerID)
            {
                KillsManager.Instance.UpdateKills(killerID);
                WinManager.Instance.PlayerKilled(killedID, killerID);

            }
            photonView.RPC("SynchronizeState", RpcTarget.AllBuffered, false);//todo se va a acumular todas las muertes, pedir su estado
            SoundManager.Instance.View.RPC("PlaySound", RpcTarget.All, photonView.ViewID, "Die");
        }
    }

    [PunRPC]
    public void SynchronizeState(bool isAlive)
    {
        _aliveBody.SetActive(isAlive);
        _deadBody.SetActive(!isAlive);
        Alive = isAlive;

        if (isAlive && photonView.IsMine) 
        {
            CanMove = true;
            HealthComponent.Heal(HealthComponent.MaxHP);
            ManaComponent.GainMana(ManaComponent.MaxMana);
        }
    }

    public void RequestRevive()
    {
        photonView.RPC("Revive", PhotonNetwork.MasterClient);
    }

    [PunRPC]
    public void Revive()
    {
        if (Alive) return;
        Alive = true;
        CanMove = true;
        HealthComponent.UpdateHP(HealthComponent.MaxHP);//cambiar por updateHP
        ManaComponent.UpdateMana(ManaComponent.MaxMana);

        photonView.RPC("SynchronizeState", RpcTarget.AllBuffered, true);
        photonView.RPC("UpdateHP", photonView.Owner, _healthComponent.CurrentHP);
        photonView.RPC("UpdateMana", photonView.Owner, _manaComponent.CurrentMana);
        SoundManager.Instance.View.RPC("PlaySound", RpcTarget.All, photonView.ViewID, "Revive");
    }


    [PunRPC]
    public void SetListener(Player player)
    {
        listener.enabled = true;
        sound_UI.Player = player;
    }

    //private void OnDestroy()
    //{
    //    if (PhotonNetwork.IsMasterClient)
    //    {
    //        WinManager.Instance.RemovePlayer(Name);
    //    }
    //}


}

