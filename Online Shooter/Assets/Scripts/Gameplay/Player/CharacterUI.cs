﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class CharacterUI : MonoBehaviourPun
{
    Character _character = null;

    [SerializeField] Image _healthBar;
    [SerializeField] Image _manaBar;
    [SerializeField] TextMeshProUGUI _healthAmount;
    [SerializeField] TextMeshProUGUI _manaAmount;

    public Character Character { get => _character; set => _character = value; }

    void UpdateHPBar() 
    {
        
        _healthBar.fillAmount = (float)Character.HealthComponent.CurrentHP / Character.HealthComponent.MaxHP;
        _healthAmount.text = Character.HealthComponent.CurrentHP + "/" + Character.HealthComponent.MaxHP;
    }

    void UpdateManaBar()
    {
        _manaBar.fillAmount = (float) Character.ManaComponent.CurrentMana / Character.ManaComponent.MaxMana;
        _manaAmount.text = Character.ManaComponent.CurrentMana + "/" + Character.ManaComponent.MaxMana;
    }

    [PunRPC]
    public void SetCharacter(int viewID)
    {
        if(Character != null)
        {
            Character.HealthComponent.OnHPChange -= UpdateHPBar;
            Character.ManaComponent.OnManaChange -= UpdateManaBar;
        }

        Character character = PhotonView.Find(viewID).gameObject.GetComponent<Character>();
        
        Character = character;
        character.HealthComponent.OnHPChange += UpdateHPBar;
        character.ManaComponent.OnManaChange += UpdateManaBar;


        UpdateHPBar();
        UpdateManaBar();

    }

}
