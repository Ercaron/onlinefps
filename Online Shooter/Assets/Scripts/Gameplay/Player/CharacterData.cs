﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "AO/Character")]
public class CharacterData : ScriptableObject
{
    [SerializeField] string className;
    [SerializeField] int maxMana;
    [SerializeField] int maxHealth;
    [SerializeField] int meleeMinDamage;
    [SerializeField] int meleeMaxDamage;
    [SerializeField] float meleeCooldown;
    [SerializeField] float speed;
    [SerializeField] List<SpellData> spells;

    public string ClassName { get => className; }
    public int MaxMana { get => maxMana;  }
    public int MaxHealth { get => maxHealth; }

    public List<SpellData> Spells { get => spells; }
    public float Speed { get => speed; }
    public float MeleeCooldown { get => meleeCooldown; set => meleeCooldown = value; }
    public int MeleeMinDamage { get => meleeMinDamage; set => meleeMinDamage = value; }
    public int MeleeMaxDamage { get => meleeMaxDamage; set => meleeMaxDamage = value; }
}
