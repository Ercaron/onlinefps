﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UIElements;
using Photon.Voice.Unity;

public class CharacterController : MonoBehaviourPun
{

    Character _char;
    Recorder _rec;
    bool _inputAvailable;

    float _prevX;
    float _prevY;

    public Character Char { get => _char; set => _char = value; }

    KeyCode _upKey;
    KeyCode _downKey;
    KeyCode _rightKey;
    KeyCode _leftKey;
    KeyCode _attackKey;
    KeyCode _hppotionKey;
    KeyCode _manapotionKey;
    KeyCode _respawnKey;
    KeyCode _danceKey;
    KeyCode _voiceKey;

    void Start()
    {
        _rec = FindObjectOfType<Recorder>();
        _inputAvailable = true;
        Keybinds.Instance.OnSettingsUpdated += UpdateSettings;
        UpdateSettings();
    }

    
    void Update()
    {
        CheckInput();
    }

    void UpdateSettings()
    {
        _upKey = Keybinds.Instance.DicKeybinds["up"];
        _downKey = Keybinds.Instance.DicKeybinds["down"];
        _rightKey = Keybinds.Instance.DicKeybinds["right"];
        _leftKey = Keybinds.Instance.DicKeybinds["left"];
        _attackKey = Keybinds.Instance.DicKeybinds["attack"];
        _hppotionKey = Keybinds.Instance.DicKeybinds["hppotion"];
        _manapotionKey = Keybinds.Instance.DicKeybinds["manapotion"];
        _respawnKey = Keybinds.Instance.DicKeybinds["respawn"];
        _danceKey = Keybinds.Instance.DicKeybinds["dance"];
        _voiceKey = Keybinds.Instance.DicKeybinds["voice"];
    }

    void CheckInput()
    {
        if (Input.GetKeyDown(KeyCode.Return)) _inputAvailable = !_inputAvailable;//OTTO-CHECK WHY AND WHAT?
        if (!_inputAvailable) return;
        var h = Input.GetAxis("Horizontal");
        var v = Input.GetAxis("Vertical");
        
        if(h != _prevX || v != _prevY)
        {
            _prevX = h;
            _prevY = v;
            _char.photonView.RPC("RequestMove", PhotonNetwork.MasterClient, h, v);
        }

        //if (Input.GetKeyDown(KeyCode.Y)) _char.Dance();
        if (Input.GetKeyDown(_respawnKey)) Char.RequestRevive();

        CastSpell();
        UsePotion();
        MeleeAttack();

        if (Input.GetKeyDown(_voiceKey))
        {
            _rec.TransmitEnabled = true;
        }
        else if (Input.GetKeyUp(_voiceKey))
        {
            _rec.TransmitEnabled = false;
        }
    }

    void CastSpell()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Char.TryCastSpell(); //Change for Click ?
        }
    }

    void UsePotion()
    {
        if (Input.GetKeyDown(_hppotionKey)) Char.RequestUsePotion("red");
        else if (Input.GetKeyDown(_manapotionKey)) Char.RequestUsePotion("blue");
    }

    void MeleeAttack()
    {
        if (Input.GetKeyUp(_attackKey))
        {
            Char.RequestHit();
        }

    }


}
