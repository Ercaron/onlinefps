﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Mana : MonoBehaviour
{
    public delegate void ManaChange();
    public event ManaChange OnManaChange;

    int _maxMana;
    int _currentMana;
    PhotonView _view;
    Character _character;
    //todo add UI object

    public int MaxMana { get { return _maxMana; } set { _maxMana = value; _currentMana = MaxMana; TriggerEvent(); } }
    public int CurrentMana { get => _currentMana; }
    public Character Character { get => _character; set => _character = value; }

    private void OnEnable()
    {
        _view = GetComponent<PhotonView>();
    }


    public void UseMana(int mana)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        _currentMana -= mana;
        if (_currentMana < 0) _currentMana = 0;
        TriggerEvent();

        Player target = null;
        GameServer.Instance.DicInverse.TryGetValue(_character, out target);
        _view.RPC("UpdateMana", target, CurrentMana);
    }

    public void GainMana(int mana)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        _currentMana += mana;
        if (_currentMana > MaxMana) _currentMana = MaxMana;
        TriggerEvent();
        Player target = null;
        GameServer.Instance.DicInverse.TryGetValue(_character, out target);
        _view.RPC("UpdateMana", target, CurrentMana);
    }

    public void UpdateMana(int newMana)
    {
        _currentMana = newMana;
        TriggerEvent();
    }

    void TriggerEvent()
    {
        if(OnManaChange != null) OnManaChange();
    }
}
