﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Health : MonoBehaviour
{
    [SerializeField] string damageUIPath;
    int _maxHP;
    int _currentHP;
    PhotonView _view;
    Character _character;

    public delegate void HPChange();
    public delegate void Death(int killedID, int killerID);
    public event HPChange OnHPChange;
    public event Death onDeath;//might need to change for parameters

    public int MaxHP { get { return _maxHP; } set { _maxHP = value; _currentHP = MaxHP; TriggerEvent(0,0); } }
    public int CurrentHP { get => _currentHP; }
    public Character Character { get => _character; set => _character = value; }

    //HACER LO MISMO QUE EN MANA

    private void OnEnable()
    {
        _view = GetComponent<PhotonView>();
    }


    public void Damage(int damage, int damagedID, int damagerID)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        _currentHP -= damage;
        TriggerEvent(damagedID,damagerID);
        Player target = null;
        GameServer.Instance.DicInverse.TryGetValue(_character, out target);
        _view.RPC("UpdateHP", target, CurrentHP);
        DamageUI ui = PhotonNetwork.Instantiate(damageUIPath, transform.position + new Vector3(0, 1, 0), Quaternion.Euler(0,0,0)).GetComponent<DamageUI>();
        ui.Follow = gameObject.transform; //todo add change color for heal
        ui.View.RPC("ChangeValue", RpcTarget.AllBuffered, damage);

      
    }

    public void Heal(int heal)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        _currentHP += heal;
        if (_currentHP > MaxHP) _currentHP = MaxHP;
        TriggerEvent(0,0);
        Player target = null;
        GameServer.Instance.DicInverse.TryGetValue(_character, out target);
        _view.RPC("UpdateHP", target, CurrentHP);
    }

    public void UpdateHP(int newHP)
    {
        _currentHP = newHP;
        TriggerEvent(0,0);
    }

    void TriggerEvent(int damagedID, int damagerID)
    {
        if (CurrentHP <= 0) onDeath(damagedID,damagerID);

        if(OnHPChange != null) OnHPChange();
        
    }

}
